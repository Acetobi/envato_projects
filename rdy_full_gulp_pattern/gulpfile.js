'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    // sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    // cleanCSS = require('gulp-clean-css'),
    // gulpif = require('gulp-if'),
    // useref = require('gulp-useref'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    clean = require('gulp-clean'),
    // rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: './app/build/',
        js: './app/build/js/',
        css: './app/build/css/',
        img: './app/build/img/',
        fonts: './app/build/fonts/'
    },
    src: { //Пути откуда брать исходники
        html: './app/src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: './app/src/js/*.js',//В стилях и скриптах нам понадобятся только main файлы
        style: './app/src/style/*.sass',
        img: './app/src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: './app/src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: './app/src/**/*.html',
        js: './app/src/js/**/*.js',
        style: './app/src/style/**/*.sass',
        img: './app/src/img/**/*.*',
        fonts: './app/src/fonts/**/*.*'
    },
    clean: './app/build'
};

//Создадим переменную с настройками нашего dev сервера
var config = {
    server: {
        baseDir: "./app/build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "gulp_full_pattern"
};

// Clean
gulp.task('clean', function () {
    return gulp.src('./app/build', {read: false})
        .pipe(clean());
});

//Напишем таск для сборки html
gulp.task('html:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(uglify()) //Сожмем наш js
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.sass
        .pipe(sass({
                outputStyle: 'compressed'
            })) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        // .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function() {
        gulp.start('html:build');
    });
    watch([path.watch.style], function() {
        gulp.start('style:build');
    });
    watch([path.watch.js], function() {
        gulp.start('js:build');
    });
    watch([path.watch.img], function() {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function() {
        gulp.start('fonts:build');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('default', ['build', 'webserver', 'watch']);